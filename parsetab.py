
# parsetab.py
# This file is automatically generated. Do not edit.
# pylint: disable=W,C,R
_tabversion = '3.10'

_lr_method = 'LALR'

_lr_signature = 'objleftEQUALSleftLPARENleftRPARENleftWORDEQUALS LPAREN NEWLINE RPAREN SPACE WORDequal : EQUALS\n             | SPACE EQUALS\n             | equal SPACE\n    lparen : LPAREN\n              | NEWLINE LPAREN\n              | lparen NEWLINE\n    rparen : RPAREN\n              | NEWLINE RPAREN\n              | rparen NEWLINE\n    kv : WORD lparen set rparenkv : WORD lparen rparenkv : WORD valueset : set NEWLINE kvvalue : value SPACE\n             | value WORD\n    set : kvvalue : equalobj : set NEWLINE'
    
_lr_action_items = {'WORD':([0,5,6,7,8,11,12,16,18,19,20,21,22,24,],[4,4,4,18,-4,-17,-1,-6,-15,-14,-5,-2,-3,4,]),'$end':([1,5,],[0,-18,]),'NEWLINE':([2,3,4,6,7,8,11,12,13,14,15,16,17,18,19,20,21,22,23,25,26,],[5,-16,9,16,-12,-4,-17,-1,-13,24,-11,-6,-7,-15,-14,-5,-2,-3,-10,-9,-8,]),'RPAREN':([3,6,7,8,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,],[-16,17,-12,-4,-17,-1,-13,17,-11,26,-7,-15,-14,-5,-2,-3,-10,26,-9,-8,]),'LPAREN':([4,9,],[8,20,]),'EQUALS':([4,10,],[12,21,]),'SPACE':([4,7,11,12,18,19,21,22,],[10,19,22,-1,-15,-14,-2,-3,]),}

_lr_action = {}
for _k, _v in _lr_action_items.items():
   for _x,_y in zip(_v[0],_v[1]):
      if not _x in _lr_action:  _lr_action[_x] = {}
      _lr_action[_x][_k] = _y
del _lr_action_items

_lr_goto_items = {'obj':([0,],[1,]),'set':([0,6,],[2,14,]),'kv':([0,5,6,24,],[3,13,3,13,]),'lparen':([4,],[6,]),'value':([4,],[7,]),'equal':([4,],[11,]),'rparen':([6,14,],[15,23,]),}

_lr_goto = {}
for _k, _v in _lr_goto_items.items():
   for _x, _y in zip(_v[0], _v[1]):
       if not _x in _lr_goto: _lr_goto[_x] = {}
       _lr_goto[_x][_k] = _y
del _lr_goto_items
_lr_productions = [
  ("S' -> obj","S'",1,None,None,None),
  ('equal -> EQUALS','equal',1,'p_space','pysfs.py',55),
  ('equal -> SPACE EQUALS','equal',2,'p_space','pysfs.py',56),
  ('equal -> equal SPACE','equal',2,'p_space','pysfs.py',57),
  ('lparen -> LPAREN','lparen',1,'p_lparen','pysfs.py',61),
  ('lparen -> NEWLINE LPAREN','lparen',2,'p_lparen','pysfs.py',62),
  ('lparen -> lparen NEWLINE','lparen',2,'p_lparen','pysfs.py',63),
  ('rparen -> RPAREN','rparen',1,'p_rparen','pysfs.py',67),
  ('rparen -> NEWLINE RPAREN','rparen',2,'p_rparen','pysfs.py',68),
  ('rparen -> rparen NEWLINE','rparen',2,'p_rparen','pysfs.py',69),
  ('kv -> WORD lparen set rparen','kv',4,'p_close_set','pysfs.py',73),
  ('kv -> WORD lparen rparen','kv',3,'p_close_empty_set','pysfs.py',78),
  ('kv -> WORD value','kv',2,'p_statement_assign','pysfs.py',83),
  ('set -> set NEWLINE kv','set',3,'p_set_assign','pysfs.py',88),
  ('value -> value SPACE','value',2,'p_value','pysfs.py',94),
  ('value -> value WORD','value',2,'p_value','pysfs.py',95),
  ('set -> kv','set',1,'p_set_start','pysfs.py',101),
  ('value -> equal','value',1,'p_value_start','pysfs.py',107),
  ('obj -> set NEWLINE','obj',2,'p_obj','pysfs.py',112),
]
