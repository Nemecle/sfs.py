#!/usr/bin/python3
#!/home/nemecle/anaconda3/bin/python3.6
# C:\Users\Nemecle\Anaconda3\python.exe
"""
reader and write a KSP save file
"""

import glob
import json
import os
import pickle
import pprint
import re
import traceback

from os import listdir
from json import JSONDecoder
from os.path import isfile, join, getmtime, basename
from datetime import datetime

def sfs_to_json(data):
    """
    convert sfs to json format

    """

    data_curled = "{" + data + "}"

    data_quoted = re.sub(r"([a-zA-Z0-9_\-/ \.+*]+) = ([^\n]+)", r'"\1" = "\2"', data_curled)
    data_name_quoted = re.sub(r"([a-zA-Z0-9_\-/\.+]+)(\n\t*)\{", r'"\1"\2{', data_quoted)
    data_noempty = re.sub(r'([a-zA-Z0-9_\-/]+) = \n', r'"\1" = ""\n', data_name_quoted)
    data_noequal = re.sub(" = ", ": ", data_noempty)
    data_name_noequal = re.sub(r'([a-zA-Z0-9_\-/"]+)(\n\t*)\{', r'\1:\2{', data_noequal)
    data_comma = re.sub(r"([\"}])(\n\t+)(?!.*})", r'\1,\2', data_name_noequal)

    return data_comma

def read_sfs(sfs):
    """
    read sfs to parse

    """

    data = ""
    line_start = 0
    line_end = -1

    with open(sfs) as f:
        data = f.readlines()

    line_end = len(data)

    result = parse_sfs(data)

    return result


def parse_sfs(sfs, global_ln=0, expected_indent=0):
    """
    parse .sfs file recursively

    """

    # print("called with block of size " + str(len(sfs)))

    base_indent = "\t" * expected_indent

    ltype = "XX"
    lenresult = ""
    message = ""
    result = []
    current_line = 0


    #for current_line in range(0, len(sfs)):
    while current_line < len(sfs):


        # empty line, EOL
        if not sfs[current_line]:
            ltype = "EOL  "
            current_line += 1
            print("END")
            return -1

        # { or }
        if match := re.search(r"^" + "\t*" + "[{}]", sfs[current_line]):
            ltype = ".."
            print(f"[{global_ln:02d}] {ltype} {sfs[current_line]}", end="")
            current_line += 1
            pass

        # key = value
        elif match := re.search(r"^" + "\t*" + "(.+) = (.*)", sfs[current_line]):
            ltype = "kv"
            result.append((match.group(1), match.group(2)))
            print(f"[{global_ln:02d}] {ltype} {sfs[current_line]}", end="")
            current_line += 1
            

        # GAME
        elif match := re.search(r"^" + "\t*" + "([A-Za-z_]+)$", sfs[current_line]):
            ltype = "BL"
            print(f"[{global_ln:02d}] {ltype} {sfs[current_line]}", end="")
            tmp_start = current_line + 1 # skip "{"
            block_offset = 2
            block = []

            # get the block with indent + 1
            while True:
                pattern = r"^" + base_indent + "\t+" + ".*$"
                ln = current_line + block_offset

                if indent_match := re.search(pattern, sfs[ln]):
                    block.append(sfs[ln])

                else:
                    break

                block_offset += 1

            # print("[DEBUG] ({}) giving block of size {}".format(match.group(1), len(block)))
            parsed_block = parse_sfs(block, global_ln+current_line, expected_indent + 1)
            result.append((match.group(1), parsed_block))

            # skip already parsed block plus parentheses
            current_line += len(block) + 2

            #print(f"END OF BLOCK {match.group(1)} of length {len(result)} \n {pprint.pprint(result)}\n")

        else:
            ltype = "ERR0R"
            print("DEBUG" + base_indent + "PUE LA MERDE: {}".format(sfs[current_line]), end="")
            current_line += 1
            pass

        # print(f"[{global_ln:02d}] ({len(result):02d}) {ltype} {sfs[current_line]}", end="")

    return result


def main():
    """
    main loop

    """

    sfsfile = "~/Documents/persistent.sfs"    

    result = read_sfs(sfsfile)

    with open(r"parsed_sfs.picke", 'wb') as pfile:
        pickle.dump(result, pfile)



    return


if __name__ == '__main__':
    main()
