#!/usr/bin/python3
#!/home/nemecle/anaconda3/bin/python3.6
# C:\Users\Nemecle\Anaconda3\python.exe
"""
test for parsed sfs data

"""

import pickle
import pprint
import random

from colorama import Fore, Back, Style, init
init(autoreset=True)

from PIL import Image  
from PIL import ImageFont
from PIL import ImageOps
from PIL import ImageDraw

WIDTH = 3000
HEIGHT = 1500
PADDING = 50
FONTSIZE = 16
FONT = "Roboto-Regular.ttf" # "DejaVuSans.ttf"

MOHOCOLOUR   = (101, 67,  33)
EVECOLOUR    = (110, 0,   110)
KERBINCOLOUR = (30,  30,  255)
MUNCOLOUR    = (90,  90,  90)
MINMUSCOLOUR = (14,  204, 100)
DUNACOLOUR   = (163, 72,  33)
DRESCOLOUR   = (122, 122, 122)
JOOLCOLOUR   = (71,  153, 38)
EELOOCOLOUR  = (171, 171, 171)
BLACK        = (0,   0,   0)
WHITE        = (255,   255,   255)

BACKGROUND = WHITE

KERBIN_DAY_IN_SECONDS =    5  * 60 * 60 \
                       +  59       * 60 \
                       +  9.4

KERBIN_YEAR_IN_SECONDS =  426 * KERBIN_DAY_IN_SECONDS \
                       +    0               * 60 * 60 \
                       +   32                    * 60 \
                       +   24.6

EXCLUDE_DEBRIS = True

def get_values(element, key):
    """
    return values based on keys

    """

    result = []

    for e in element:
        if e[0] == key:
            result.append(e[1])

    if len(result) == 1:
        return result[0]

    return result


def orbitref_to_soi(orbitref):
    """translate REF to soi"""

    if orbitref == "0":
        return "interplanetary"
    
    elif orbitref == "1":
        return "Kerbin"
    
    elif orbitref == "2":
        return "Mun"
    
    elif orbitref == "3":
        return "Minmus"
    
    elif orbitref == "4":
        return "Moho"
    
    elif orbitref == "5":
        return "Eve"
    
    elif orbitref == "6":
        return "Duna"

    # speculative beyond here
    elif orbitref == "7":
        return "Dres"
        
    elif orbitref == "8":
        return "Jool"
        
    elif orbitref == "9":
        return "Eeloo"
        
    else:
        return -1        

def to_readable_time(t):
    """
    convert a xxxx.xxxx (str) time to x days, x hours, etc
    WARNING APPROXIMATE
    kerbin_period = 426 d 0 h 32 m 24.6 s
    kerbin    day = 5 h 59 m 9.4 s

    t -- str time like 73698562.208508149

    return tuple (years, days, hours, minutes, seconds)

    """

    ftime = float(t)

    years = int(ftime / float(KERBIN_YEAR_IN_SECONDS))
    days  = int((ftime - years * KERBIN_YEAR_IN_SECONDS) \
            / float(KERBIN_DAY_IN_SECONDS))

    hours  = int((ftime - years * KERBIN_YEAR_IN_SECONDS \
                        -  days *  KERBIN_DAY_IN_SECONDS) \
            / (60 * 60))

    minutes  = int((ftime - years * KERBIN_YEAR_IN_SECONDS \
                        -    days *  KERBIN_DAY_IN_SECONDS
                        -   hours * 60 * 60) \
            / 60)

    seconds  = int((ftime - years * KERBIN_YEAR_IN_SECONDS \
                        -    days *  KERBIN_DAY_IN_SECONDS
                        -   hours * 60 * 60                  \
                        -   minutes    * 60))

    return (years, days, hours, minutes, seconds)

def type_to_symbol(vesseltype):
    """return an emoji based on type"""

    if vesseltype == "Probe":
        vtype = "🛰️"

    elif vesseltype == "Ship":
        vtype = "🚀"

    elif vesseltype == "Rover":
        vtype = "🚙"

    elif vesseltype == "Debris":
        vtype = "🔩"

    elif vesseltype == "Flag":
        vtype = "🏳️"

    elif vesseltype == "Lander":
        vtype = "🔭"

    elif vesseltype == "SpaceObject":
        vtype = "☄️"

    elif vesseltype == "EVA":
        vtype = "🧑‍🚀"

    elif vesseltype == "Station":
        vtype = "🗜️"

    elif vesseltype == "Relay":
        vtype = "📡"

    elif vesseltype == "Base":
        vtype = "⛺"

    elif vesseltype == "Unknown":
        vtype = "❓"

    else:
        vtype = "?"

    return vtype

def list_by_soi(vessels):
    """


    """

    kerbol_system = {
      "interplanetary": [],
      "Kerbin": [],
      "Mun": [],
      "Minmus": [],
      "Moho": [],
      "Eve": [],
      "Duna": [],
      "Dres": [],
      "Jool": [],
      "Eeloo": [],
    }

    for vessel in vessels:
        name = get_values(vessel, "name")
        vesseltype = get_values(vessel, "type")

        orbit = get_values(vessel, "ORBIT")
        soi = orbitref_to_soi(get_values(orbit, "REF"))
        situation = get_values(vessel, "sit")


        part_count = str((len(get_values(vessel, "PART"))))

        met = get_values(vessel, "met")

        days = int(float(met) )

        lct = get_values(vessel, "lct")
        lct_year = lct


        launch_time = int(float(lct) / 60 / 60 / 6)

        kerbol_system[soi].append(
            [
              situation,
              vesseltype,
              name,
              lct,
              part_count,
            ]
            )

        #print(f"{name}\n------\ntype: {vesseltype}\non: {soi}\npart count: {part_count}\nmet: {met}\nlaunch_time: {launch_time}\n\n")
        # pprint.pprint(kerbol_system)

    for soi, vessels in kerbol_system.items():
        print(soi)
        for vessel in vessels:
            situation = "   "
            if vessel[0] == "LANDED":
                situation = "[L]"

            elif vessel[0] == "SPLASHED":
                situation = "[S]"

            elif vessel[0] == "ORBITING":
                situation = "   "

            else:
                situation = vessel[0]

            vtype = type_to_symbol(vessel[1])

            lct_year, lct_day, lct_hour, lct_min, lct_sec = to_readable_time(vessel[3])
            launch_time = f"Y{lct_year}d{lct_day}-{lct_hour}h{lct_min}"

            print(f'    {situation}{vtype} {vessel[2]} ({vessel[4]} parts), launched {launch_time}')

    return

def chronology(vessels):
    """


    """

    vessels_list = []


    for vessel in vessels:
        name = get_values(vessel, "name")
        vesseltype = get_values(vessel, "type")

        orbit = get_values(vessel, "ORBIT")
        soi = orbitref_to_soi(get_values(orbit, "REF"))
        situation = get_values(vessel, "sit")

        part_count = str((len(get_values(vessel, "PART"))))

        met = get_values(vessel, "met")

        lct = get_values(vessel, "lct")
        lct_year = lct


        vessels_list.append(
            {
              "lct": lct,
              "name": name,
              "situation": situation,
              "vesseltype": vesseltype,
              "soi": soi,
              "part_count": part_count,
            }
        )

    chronology = sorted(vessels_list, key = lambda i: i['lct'])
    # chronology = sorted(vessels_list, key = lambda i: (i['lct'], i['soi']))
    # chronology = sorted(vessels_list, key = lambda i: i['lct'],reverse=True)

    for vessel in vessels_list:
        lct_year, lct_day, lct_hour, lct_min, lct_sec = to_readable_time(vessel["lct"])

        if vessel["soi"] == "interplanetary":
            colour = ""
        
        elif vessel["soi"] == "Kerbin":
            colour = Fore.BLUE
        
        elif vessel["soi"] == "Mun":
            colour = Style.DIM
        
        elif vessel["soi"] == "Minmus":
            colour = ""
        
        elif vessel["soi"] == "Moho":
            colour = ""
        
        elif vessel["soi"] == "Eve":
            colour = Fore.MAGENTA
        
        elif vessel["soi"] == "Duna":
            colour = Fore.RED

        # speculative beyond here
        elif vessel["soi"] == "Dres":
            colour = ""
            
        elif vessel["soi"] == "Jool":
            colour = Fore.GREEN
            
        elif vessel["soi"] == "Eeloo":
            colour = Fore.WHITE


        print(colour + f'Y{lct_year}d{lct_day:03d} {vessel["name"]}')


    # lct_year, lct_day, lct_hour, lct_min, lct_sec = to_readable_time(vessel[3])
    # launch_time = f"Y{lct_year}d{lct_day}-{lct_hour}h{lct_min}"

    return


def graphic_chronology(vessels):
    """
    Graphic chronology using matplotlib

    """

    vessels_list = []

    for vessel in vessels:
        name = get_values(vessel, "name")
        vesseltype = get_values(vessel, "type")

        orbit = get_values(vessel, "ORBIT")
        soi = orbitref_to_soi(get_values(orbit, "REF"))
        situation = get_values(vessel, "sit")

        part_count = str((len(get_values(vessel, "PART"))))

        met = get_values(vessel, "met")

        lct = get_values(vessel, "lct")
        lct_year = lct

        if EXCLUDE_DEBRIS and (vesseltype == "Debris" or vesseltype == "SpaceObject"):
            continue

        vessels_list.append(
            {
              "lct": lct,
              "name": name,
              "situation": situation,
              "vesseltype": vesseltype,
              "soi": soi,
              "part_count": part_count,
            }
        )

    chronology = sorted(vessels_list, key = lambda i: i['lct'])
    # chronology = sorted(vessels_list, key = lambda i: (i['lct'], i['soi']))
    # chronology = sorted(vessels_list, key = lambda i: i['lct'],reverse=True)

    last_launch = chronology[-1]["lct"]

    img  = Image.new( mode = "RGB", size = (WIDTH, HEIGHT), color = BACKGROUND)
    draw = ImageDraw.Draw(img)
    font = ImageFont.truetype(FONT, FONTSIZE)

    width, height = img.size
    # draw.text((0, 0),"Sample Text",(0,0,0),font=font)

    # draw scale
    draw.line([(PADDING, height - PADDING), (width - PADDING, height - PADDING)], fill=(0, 0, 0), width=2)

    for i in range(0, 10):
        tY = i * KERBIN_YEAR_IN_SECONDS
        lmax = WIDTH - PADDING*2
        tmax = 10 * KERBIN_YEAR_IN_SECONDS

        y_position = height - PADDING + FONTSIZE
        x_position = PADDING + lmax/tmax * tY
        txt = f"Y{str(i)}"

        # print(f"x:{x_position} y:{y_position} tY:{tY}")

        draw.text((x_position, y_position), txt, BLACK,font=font)


    previous_time = (0,0)
    stage = 0
    stages = [PADDING] # at which level to print, and create them as needed

    for i, vessel in enumerate(vessels_list):
        lct_year, lct_day, lct_hour, lct_min, lct_sec = to_readable_time(vessel["lct"])

        ldate = f'{lct_year}-{lct_day:03d}'

        colour = BLACK

        if vessel["soi"] == "interplanetary":
            colour = BLACK 
        
        elif vessel["soi"] == "Kerbin":
            colour = KERBINCOLOUR 
        
        elif vessel["soi"] == "Mun":
            colour = MUNCOLOUR 
        
        elif vessel["soi"] == "Minmus":
            colour = MINMUSCOLOUR 
        
        elif vessel["soi"] == "Moho":
            colour = MOHOCOLOUR 
        
        elif vessel["soi"] == "Eve":
            colour = EVECOLOUR 
        
        elif vessel["soi"] == "Duna":
            colour = DUNACOLOUR 

        # speculative beyond here
        elif vessel["soi"] == "Dres":
            colour = DRESCOLOUR 
            
        elif vessel["soi"] == "Jool":
            colour = JOOLCOLOUR 
            
        elif vessel["soi"] == "Eeloo":
            colour = EELOOCOLOUR 
            
        else:
            colour = BLACK

        # txt = Image.new('L', font.getsize(vessel["name"]))
        # draw_txt = ImageDraw.Draw(txt)
        # draw_txt.text( (0, 0), "Someplace Near Boulder",  font=font)
        # w=txt.rotate(17.5,  expand=1)

        # img.paste(ImageOps.colorize(w, (0,0,0), (255,255,84)), (242,60),  w)


        # draw.text((0, i * FONTSIZE),vessel["name"], colour,font=font)

        lct = float(vessel["lct"])
        lmax = WIDTH - PADDING*2
        tmax = 10 * KERBIN_YEAR_IN_SECONDS

        txt = f'{type_to_symbol(vessel["vesseltype"])} {vessel["name"]} ({vessel["part_count"]})'

        previous_year, previous_day = previous_time

        if lct_year == previous_year and lct_day == previous_day:
            stage += 1
        else:
            previous_time = lct_year, lct_day
            stage = 0

        x_position = PADDING + lmax/tmax * lct

        stage = -1
        for k, s in enumerate(stages):
            if stages[k] < x_position:
                stage = k
                stages[k] = x_position + font.getsize(txt)[0]
                break
            else:
                continue

        if stage == -1:
            stage = len(stages)
            stages.append(PADDING)

        #print(f"s: {stage} {vessel['name']}")


        y_position = HEIGHT - 50 - FONTSIZE * 2 - stage * (FONTSIZE + 4)

        draw.line([(x_position - 10, y_position + FONTSIZE + 2), (x_position - 10, height - PADDING)], colour, width=1)

    previous_time = (0,0)
    stage = 0
    stages = [PADDING] # at which level to print, and create them as needed


    for i, vessel in enumerate(vessels_list):
        lct_year, lct_day, lct_hour, lct_min, lct_sec = to_readable_time(vessel["lct"])

        ldate = f'{lct_year}-{lct_day:03d}'

        colour = BLACK

        if vessel["soi"] == "interplanetary":
            colour = BLACK 
        
        elif vessel["soi"] == "Kerbin":
            colour = KERBINCOLOUR 
        
        elif vessel["soi"] == "Mun":
            colour = MUNCOLOUR 
        
        elif vessel["soi"] == "Minmus":
            colour = MINMUSCOLOUR 
        
        elif vessel["soi"] == "Moho":
            colour = MOHOCOLOUR 
        
        elif vessel["soi"] == "Eve":
            colour = EVECOLOUR 
        
        elif vessel["soi"] == "Duna":
            colour = DUNACOLOUR 

        # speculative beyond here
        elif vessel["soi"] == "Dres":
            colour = DRESCOLOUR 
            
        elif vessel["soi"] == "Jool":
            colour = JOOLCOLOUR 
            
        elif vessel["soi"] == "Eeloo":
            colour = EELOOCOLOUR 
            
        else:
            colour = BLACK

        # txt = Image.new('L', font.getsize(vessel["name"]))
        # draw_txt = ImageDraw.Draw(txt)
        # draw_txt.text( (0, 0), "Someplace Near Boulder",  font=font)
        # w=txt.rotate(17.5,  expand=1)

        # img.paste(ImageOps.colorize(w, (0,0,0), (255,255,84)), (242,60),  w)


        # draw.text((0, i * FONTSIZE),vessel["name"], colour,font=font)

        lct = float(vessel["lct"])
        lmax = WIDTH - PADDING*2
        tmax = 10 * KERBIN_YEAR_IN_SECONDS

        txt = f'{type_to_symbol(vessel["vesseltype"])} {vessel["name"]} ({vessel["part_count"]})'

        previous_year, previous_day = previous_time

        if lct_year == previous_year and lct_day == previous_day:
            stage += 1
        else:
            previous_time = lct_year, lct_day
            stage = 0

        x_position = PADDING + lmax/tmax * lct

        stage = -1
        for k, s in enumerate(stages):
            if stages[k] < x_position:
                stage = k
                stages[k] = x_position + font.getsize(txt)[0]
                break
            else:
                continue

        if stage == -1:
            stage = len(stages)
            stages.append(x_position + font.getsize(txt)[0])

        #print(f"s: {stage} {vessel['name']}")


        y_position = HEIGHT - 50 - FONTSIZE * 2 - stage * (FONTSIZE + FONTSIZE/2)

        txt_width,txt_height = font.getsize(txt)

        draw.rectangle([(x_position, y_position),(x_position  + txt_width, y_position + txt_height)], fill=BACKGROUND,width=1)
        draw.line([(x_position - 10, y_position + FONTSIZE + 2), (x_position - 10 + len(txt) * (FONTSIZE/3), y_position + FONTSIZE + 2)], colour, width=1)
        draw.text((x_position, y_position),txt, colour,font=font)

    # img.show()
    img.save("/tmp/ksp_time.jpg")


def main():
    """
    main loop

    """

    pfile = ""



    with open(r"parsed_sfs.picke", "rb") as pfile:
        data = pickle.load(pfile)

    flightstate = get_values(data[0][1], "FLIGHTSTATE")
    # print(str(flightstate))

    vessels = get_values(flightstate, "VESSEL")

    # list_by_soi(vessels)

    graphic_chronology(vessels)

    return


if __name__ == '__main__':
    main()
