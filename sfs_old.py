#!/usr/bin/python3
#!/home/nemecle/anaconda3/bin/python3.6
# C:\Users\Nemecle\Anaconda3\python.exe
"""
reader and write a KSP save file
"""

import glob
import os
import re
import json
import traceback

from os import listdir
from json import JSONDecoder
from os.path import isfile, join, getmtime, basename
from datetime import datetime

def sfs_to_json(data):
    """
    convert sfs to json format

    """

    data_curled = "{" + data + "}"

    data_quoted = re.sub(r"([a-zA-Z0-9_\-/ \.+*]+) = ([^\n]+)", r'"\1" = "\2"', data_curled)
    data_name_quoted = re.sub(r"([a-zA-Z0-9_\-/\.+]+)(\n\t*)\{", r'"\1"\2{', data_quoted)
    data_noempty = re.sub(r'([a-zA-Z0-9_\-/]+) = \n', r'"\1" = ""\n', data_name_quoted)
    data_noequal = re.sub(" = ", ": ", data_noempty)
    data_name_noequal = re.sub(r'([a-zA-Z0-9_\-/"]+)(\n\t*)\{', r'\1:\2{', data_noequal)
    data_comma = re.sub(r"([\"}])(\n\t+)(?!.*})", r'\1,\2', data_name_noequal)

    return data_comma


def parse_object_pairs(pairs):
    """
    https://stackoverflow.com/questions/29321677/python-json-parser-allow-duplicate-keys
    """
    return pairs

def my_obj_pairs_hook (lst):
    """
    turn a serie of duplicate keys into a list

    https://www.tutorialfor.com/blog-260968.htm

    """

    result=()
    count=()

    for key, val in lst:
        if key in count:
            count[key] = 1 + count[key]
        else:
            count[key] = 1

        if key in result:
            if count[key] > 2:
                result[key].append(val)

            else:
                result[key] = [result[key], val]

        else:
            result[key] = val

    return result

def json_decoder(data):
    """
    parse JSON while allowing duplicate keys
    https://stackoverflow.com/questions/29321677/python-json-parser-allow-duplicate-keys

    """

    decoder = JSONDecoder(object_pairs_hook=parse_object_pairs)
    obj = decoder.decode(data)

    return obj


def reader(savefile):
    """
    read the content of a .sfs file

    """

    return


def main():
    """
    main loop

    """

    data = ""

    with open("persistent.sfs") as f:
        data = f.read()


    print(str(len(data)))

    data_processed = sfs_to_json(data)

    # json_data = json_decoder(data_processed)
    save = json.loads(data_processed, object_pairs_hook=my_obj_pairs_hook)

    with open("data.json", "w") as fw:
       # fw.write(json.dumps(json_data, indent=4))
       fw.write(save)


    print(json_data)

    return


if __name__ == '__main__':
    main()
